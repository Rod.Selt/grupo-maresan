<?session_start();
/**
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */

// HTTP headers for no cache etc
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");



$id=$_GET['id'];
$nt=$_GET['nt'];
$idt=$_GET['idt'];
$ni=$_GET['ni'];
$anT=$_GET['anT'];
$alT=$_GET['alT'];
$anN=$_GET['anN'];
$alN=$_GET['alN'];
include("../../../variables.php");
conectar();

// Settings
//$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
//$targetDir = 'uploads/';
$targetDir = '../../../fotos3/';

//$cleanupTargetDir = false; // Remove old files
//$maxFileAge = 60 * 60; // Temp file age in seconds

// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Get parameters
$chunk = isset($_REQUEST["chunk"]) ? $_REQUEST["chunk"] : 0;
$chunks = isset($_REQUEST["chunks"]) ? $_REQUEST["chunks"] : 0;
$fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';
$fileName = $_GET['ni'] . $fileName;

// Clean the fileName for security reasons
$fileName = preg_replace('/[^\w\._]+/', '', $fileName);
if($fileName!=$_SESSION['unique_name']){
    $nombreFoto_thumb = "fotos3/thumb/t" . $fileName; 
    $nombreFoto = "fotos3/" . $fileName;
    $resultInicial=mysql_query("SELECT " . $idt . " FROM ". $nt ." WHERE ".$idt." = '$id'");
    $cantidadInicial=mysql_num_rows($resultInicial);
    {$inicial=1;}
    if ($cantidadInicial>0)
        {$inicial=0;}
    mysql_free_result($resultInicial);
    
    $resultValidar=mysql_query("SELECT " . $idt . " FROM ". $nt ." WHERE url = '$nombreFoto' AND $idt = '$id'");
    $cantidadHubo=mysql_num_rows($resultValidar);
    mysql_free_result($resultValidar);
    if ($cantidadHubo==0)
        {
        if(mysql_query("INSERT INTO " . $nt . " (url, thumb, " . $idt . ", descripcion, inicial) VALUES ('" . $nombreFoto . "','" . $nombreFoto_thumb . "', " . $id . ",'" . $descripcion . "', " . $inicial . ")") == false)
         {
              echo "Ocurrio un Error al Almacenar los Datos en la Base de Datos<br>";
              echo "INSERT INTO " . $nt . " (url, thumb, " . $idt . ", descripcion, inicial) VALUES ('" . $nombreFoto . "','" . $nombreFoto_thumb . "', " . $id . ",'" . $descripcion . "', " . $inicial . ")<br>";
         }
        }
    
    $_SESSION['unique_name']=$fileName;
}
mysql_close();

// Make sure the fileName is unique but only if chunking is disabled
if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
	$ext = strrpos($fileName, '.');
	$fileName_a = substr($fileName, 0, $ext);
	$fileName_b = substr($fileName, $ext);

	$count = 1;
	while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
		$count++;

	$fileName = $fileName_a . '_' . $count . $fileName_b;
}

// Create target dir
if (!file_exists($targetDir))
	@mkdir($targetDir);

// Look for the content type header
if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
	$contentType = $_SERVER["HTTP_CONTENT_TYPE"];

if (isset($_SERVER["CONTENT_TYPE"]))
	$contentType = $_SERVER["CONTENT_TYPE"];

// Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
if (strpos($contentType, "multipart") !== false) {
	if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
        
        // Open temp file
		$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
		if ($out) {
			// Read binary input stream and append it to temp file
			$in = fopen($_FILES['file']['tmp_name'], "rb");

			if ($in) {
				while ($buff = fread($in, 4096))
					fwrite($out, $buff);
			} else
				die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
			fclose($in);
			fclose($out);
            
			@unlink($_FILES['file']['tmp_name']);

		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
	} else
		die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
} else {
	// Open temp file
	$out = fopen($targetDir . DIRECTORY_SEPARATOR . $fileName, $chunk == 0 ? "wb" : "ab");
	if ($out) {
		// Read binary input stream and append it to temp file
		$in = fopen("php://input", "rb");

		if ($in) {
			while ($buff = fread($in, 4096))
				fwrite($out, $buff);
		} else
			die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

		fclose($in);
		fclose($out);
	} else
		die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}

//Redimnesiono para el thumb
require_once '../../thumbing/ThumbLib.inc.php';
$thumb = PhpThumbFactory::create($targetDir . DIRECTORY_SEPARATOR . $fileName);
$thumb->resize($anT, $alT);
$thumb->save('../../../fotos3/thumb/t'.$fileName);//LE paso el nombre asi, porq si utilizara el $nombreFoto_thumb deberia ser por session o inventar algo.
 //$nombreFoto_thumb = "fotos3/thumb/t" . $fileName;
 //fin armado thumb


// Return JSON-RPC response
//die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
die('{"jsonrpc" : "2.0", "result" : "ok", "id" : "id"}');
//die('{"jsonrpc" : "2.0", "result" : "'. $_SESSION['nf'] . '", "id" : "id"}');



?>