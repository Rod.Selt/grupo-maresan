<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Plupload - Queue widget example</title>
<style type="text/css">
	body {
		font-family:Verdana, Geneva, sans-serif;
		font-size:13px;
		color:#333;
		background:url(../bg.jpg);
	}
</style>
<link rel="stylesheet" href="../../js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css" media="screen" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<!--<script type='text/javascript' src='../../../../jquerycontact/js/jquery.js'></script>-->

<script type="text/javascript" src="http://bp.yahooapis.com/2.4.21/browserplus-min.js"></script>

<script type="text/javascript" src="../../js/plupload.js"></script>
<script type="text/javascript" src="../../js/es.js"></script>
<script type="text/javascript" src="../../js/plupload.gears.js"></script>
<script type="text/javascript" src="../../js/plupload.silverlight.js"></script>
<script type="text/javascript" src="../../js/plupload.flash.js"></script>
<script type="text/javascript" src="../../js/plupload.browserplus.js"></script>
<script type="text/javascript" src="../../js/plupload.html4.js"></script>
<script type="text/javascript" src="../../js/plupload.html5.js"></script>
<script type="text/javascript" src="../../js/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<!-- <script type="text/javascript"  src="http://getfirebug.com/releases/lite/1.2/firebug-lite-compressed.js"></script> -->
</head>
<body>

<form method="post" action="dump.php">
    <?
    //trae la info de: id=Id del campo, nt=nombre de la tabla fotos, idt=nombre de la FK existente en la tabla fotos, ni=nombre imagen (es el textito q coloco para diferenciar entre imagenes de distintos 'objetos')
    //anT=ancho thumbail, alT=alto thumbial, anN=ancho foto Normal, alN=alto foto Normal.
    //en pagref traigo la pag donde debe VOLVER
    $id=$_GET['id'];
    $nt=$_GET['nt'];
    $idt=$_GET['idt'];
    $ni=$_GET['ni'];
    $anT=$_GET['anT'];
    $alT=$_GET['alT'];
    $anN=$_GET['anN'];
    $alN=$_GET['alN'];
    if ($anN=="") {$anN=800;}
    if ($alN=="") {$alN=600;};
    $paginaRefrezco=$_GET['pagref'];
    ?>
	<!--<h1>Subiendo file</h1>-->
    <!--<div id="images"></div>-->
	<!--<p>Shows the jQuery Plupload Queue widget and under different runtimes.</p>-->
	<div style="float: left; margin: 5px">
		<!--<h3>En Flash</h3>-->
		<div id="flash_uploader" style="width: 700px; height: 330px;">Tu navegador no posee Flash Instalado.</div>
	</div>
	<br style="clear: both" />
	<!--<input type="submit" value="Send" />-->
</form>

<script type="text/javascript">
var image_count = 0;

$(function() {
	// Setup flash version
	$("#flash_uploader").pluploadQueue({
		// General settings
		runtimes : 'flash',
		url : '../upload.php?id=<?=$id;?>&nt=<?=$nt;?>&idt=<?=$idt;?>&ni=<?=$ni;?>&anT=<?=$anT;?>&alT=<?=$alT;?>&anN=<?=$anN;?>&alN=<?=$alN;?>',
        //url : '../upload.php',
		max_file_size : '10mb',
		chunk_size : '1mb',
		unique_names : true,
        multiple_queues : true,
        sortable: true,
        // adding this for redirecting to page once upload complete
        //preinit: attachCallbacks,
        
		filters : [
			{title : "Imagenes", extensions : "jpg,gif,png"}//,
			//{title : "Zip files", extensions : "zip"}
		],
        init: {
          FileUploaded: function(up, file, response) {
            image_count++;
            var obj = jQuery.parseJSON(response.response);
            if((obj.result) == "ok") 
                {
                if (this.files.length == image_count) //si la cantidad subida es igual a la cant total a subir, refrezco la pag
                    {
                    //parent.location.reload(); 
                    parent.location.href= '<?=$paginaRefrezco;?>';
                    }
                }
          }
        },
        
        
		// Resize images on clientside if we can
        resize : {width : <?=$anN;?>, height : <?=$alN;?>, quality : 90},
		//resize : {width : 320, height : 240, quality : 90},

		// Flash settings
		flash_swf_url : '../../js/plupload.flash.swf',
        
	});


});


function attachCallbacks(Uploader) {

Uploader.bind('FileUploaded', function(Up, File, Response) {

  if( (Uploader.total.uploaded + 1) == Uploader.files.length)
  {
    window.location = '[url]http://www.google.com[/url]';
  }

});
}
</script>

</body>
</html>



